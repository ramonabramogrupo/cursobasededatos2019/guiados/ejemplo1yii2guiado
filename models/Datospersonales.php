<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datospersonales".
 *
 * @property int $id
 * @property string $nombre
 * @property int $edad
 * @property double $notaFinal
 * @property string $fechaNacimiento
 * @property string $estado
 *
 * @property Notas[] $notas
 */
class Datospersonales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datospersonales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['edad'], 'integer'],
            [['notaFinal'], 'number'],
            [['fechaNacimiento'], 'safe'],
            [['nombre', 'estado'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'notaFinal' => 'Nota Final',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Notas::className(), ['alumno' => 'id']);
    }
}
