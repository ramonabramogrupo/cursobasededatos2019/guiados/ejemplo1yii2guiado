<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notas".
 *
 * @property int $id
 * @property int $alumno
 * @property double $nota
 * @property string $fechaExamen
 *
 * @property Datospersonales $alumno0
 */
class Notas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno'], 'integer'],
            [['nota'], 'number'],
            [['fechaExamen'], 'safe'],
            [['alumno', 'fechaExamen'], 'unique', 'targetAttribute' => ['alumno', 'fechaExamen']],
            [['alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Datospersonales::className(), 'targetAttribute' => ['alumno' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alumno' => 'Alumno',
            'nota' => 'Nota',
            'fechaExamen' => 'Fecha Examen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno0()
    {
        return $this->hasOne(Datospersonales::className(), ['id' => 'alumno']);
    }
}
