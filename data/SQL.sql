﻿DROP DATABASE IF EXISTS alumnos;
CREATE DATABASE alumnos;

USE alumnos;

CREATE OR REPLACE TABLE datosPersonales(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  edad int,
  notaFinal float,
  fechaNacimiento date,
  estado varchar(50),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE notas(
  id int AUTO_INCREMENT,
  alumno int,
  nota float,
  fechaExamen date,
  PRIMARY KEY(id),
  UNIQUE (alumno,fechaExamen),
  CONSTRAINT FKNotasAlumno FOREIGN KEY (alumno) REFERENCES datosPersonales(id) on DELETE CASCADE on UPDATE CASCADE
  );


INSERT INTO datosPersonales (nombre, edad, notaFinal, fechaNacimiento)
  VALUES ('Ana', 0, 0, '2000/1/1'),('Rosa',0,0,'2001/4/8');

INSERT INTO notas (alumno, nota, fechaExamen)
  VALUES (1, 4, '2019/8/1'),(1, 6, '2019/8/2'),(1, 8, '2019/7/1'),(1, 9, '2019/9/2'),(2, 8, '2019/8/1'),(2, 6, '2019/8/2'),(2, 8, '2019/7/1');

