<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Gestion Alumnos';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Gestion de Alumnos</h1>

        <p class="lead">Gestion basica</p>
        <div class="panel panel-info">
            <div class="panel-body">
                <p><?= Html::a("Gestion de los Alumnos", ["/datos-personales"], ["class" => "btn btn-lg btn-success"]) ?></p>
                <p><?= Html::a("Gestion de las Notas", ["/notas"], ["class" => "btn btn-lg btn-success"]) ?></p>
            </div>
        </div>
    </div>
</div>
